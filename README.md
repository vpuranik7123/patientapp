### Patient App 
I am hosting the app on free tire of heroku. 


#### Heroku Hosted
- [WebApp](https://) (Yet to do)


## Angular 6 Frontend with SpringBoot (Java) Backend

### Technology Stack
Component         | Technology
---               | ---
Frontend          | [Angular 6](https://github.com/angular/angular)
Backend (REST)    | [SpringBoot](https://projects.spring.io/spring-boot) (Java)
Security          | Token Based Using Auth0 (Spring Security and [JWT](https://github.com/auth0/java-jwt) )
REST Spec         | [Open API Standard](https://www.openapis.org/) 
In Memory cache   | Apache Ignite (https://ignite.apache.org/)  // could have used Map (from java.util package ) went for apache ignite as it has really cool features
Client Build Tools| [angular-cli](https://github.com/angular/angular-cli), Webpack, npm
Server Build Tools| Maven(Java) 

## Folder Structure
```bash
PROJECT_FOLDER
│  README.md
│  pom.xml           
└──[src]      
│  └──[main]      
│     └──[java]      
│     └──[resources]
│        └── application.properties #contains springboot cofigurations
│
└──[target]              
│  └──[classes]
│	  └──[com]
│	  │ └──doctoranywhere  #Java build files, auto-created after running java build: mvn install
│     └──[public]
│     	  └──[doctoranywhere-ui]         
│                        
│
└──[doctoranywhere-ui]
   │  package.json     
   │  angular-cli.json   #ng build configurations)
   └──[node_modules]
   └──[src]              #frontend source files
   └──[dist]             #frontend build files, auto-created after running angular build: ng -build
```

## Prerequisites
Ensure you have this installed before proceeding further
- Java 8
- Maven 3.3.9+ o
- Node 8.0 or above,  
- npm 5 or above,   
- Angular-cli 1.6.3

## About
This is an RESTfull implementation of Patient details app.
The goal of the project is to 
- Perform basic CRUD operations on the Patient details.
- Securing REST full APIs using [SpringBoot](https://projects.spring.io/spring-boot)
- Consume RESTfull service and make an HTML5 based Single Page App using [Angular 6](https://github.com/angular/angular)

### Features of the Project
* Backend
  -> Expose Restful API via Token Based authentication (using Spring security)
  -> Pagination

* Frontend
  -> View Patient Details
  -> CRUD operations.
  -> Filtering and Sorting

* Build
  -> Build all in one app that includes (RESTfull API, frontend and security)
  -> Portable app, Ideal for dockers, cloud hosting.

## Apache Ignite (In memory Cache)
I have utlized Apache Ignite's Cache for all in-memory requirements of  the application. 

## Spring security
Security is **enabled** by default, to disable, you must comment [this line](./src/main/java/com/doctoranywhere/app/security/SecurityConfiguration.java#L11) in `/src/main/java/com/doctoranywhere/app/security/SecurityConfiguration`<br/>
When security is enabled, none of the REST API will be accessesble directly.

To test security access `http://localhost:8080/patient/getall` API and you should get a forbidden/Access denied error. 
In order to access these secured API you must first obtain a token. This would be handled by Auth0.

### Build Frontend (optional step)
Code for frontend is allready compiled and saved under the ```doctoranywhere-ui/dist``` 
when building the backend app (using maven) it will pickup the code from ```doctoranywhere-ui/dist```. However if you modified the frontend code and want your changes to get reflected then you must build the frontend 

# Navigate to PROJECT_FOLDER/doctoranywhere-ui (should contain package.json )
npm install
# build the project (this will put the files under dist folder)
ng build --prod --aot=true
```
### Build Backend (SpringBoot Java)

Maven Build : Navigate to the root folder where pom.xml is present 
mvn clean install

### Start the API and WebUI server

 If you build with maven jar location will be 
java -jar ./target/assignment-0.0.1-SNAPSHOT.jar

### Accessing Application
Component         | URL                                     
---               | ---                                     
Frontend          |  http://localhost:4200/patients 