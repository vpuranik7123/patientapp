package com.doctoranywhere.app;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.doctoranywhere.app.model.Patient;
import com.doctoranywhere.app.service.AddressService;
import com.doctoranywhere.app.service.PatientCrudService;

/*
 * 
 * For loading random data
 * 
 */
@Component
public class AppRunner implements ApplicationRunner {
	
	@Autowired
	PatientCrudService patientService;
	
	@Autowired
	AddressService addressService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		/*Address a1 = new Address();
		Address a2 = new Address();
		Address a3 = new Address();
		Address a4 = new Address();
		Address a5 = new Address();
		
		a1.setAddressLine1("Brookyln Street");
		a1.setAddressLine2("1037, NY EXE");
		a1.setCity("New York");
		a1.setCountry("USA");
		a1.setPostalCode("654790");
		
		
		a2.setAddressLine1("Dharavi");
		a2.setAddressLine2("Juhu lake");
		a2.setCity("Bombay");
		a2.setCountry("India");
		a2.setPostalCode("789987");
		
		a3.setAddressLine1("Mt Abhu");
		a3.setAddressLine2("Nepal Street");
		a3.setCity("Kathamandu");
		a3.setCountry("Nepal");
		a3.setPostalCode("189987");
		
		
		a4.setAddressLine1("Times Square");
		a4.setAddressLine2("NY Street");
		a4.setCity("New York");
		a4.setCountry("USA");
		a4.setPostalCode("109987");
	
		
		a5.setAddressLine1("Calfornia view");
		a5.setAddressLine2("Bervely Hills");
		a5.setCity("LA");
		a5.setCountry("USA");
		a5.setPostalCode("189987");*/
		
		
		Patient p1 = new Patient();
		final String uuid = UUID.randomUUID().toString().replace("-", "");
		System.out.println(uuid);
		//a1.setPatientId(uuid);
		//a2.setPatientId(uuid);
		
		p1.setId(uuid);
		p1.setFirstName("John");
		p1.setLastName("Doe");
		p1.setContactNumber("232-323-1879");
		p1.setActive(true);
		p1.setAddressLine1("Brookyln Street");
		p1.setAddressLine2("1037, NY EXE");
		p1.setCity("New York");
		p1.setCountry("USA");
		p1.setPostalCode("654790");
	
		
		
		
		patientService.add(p1);
		//addressService.addAddressToRepo(a1);
		//addressService.addAddressToRepo(a2);
		Patient p2 = new Patient();
		final String uuid2 = UUID.randomUUID().toString().replace("-", "");
		System.out.println(uuid2);
		p2.setId(uuid2);
		p2.setFirstName("Mary");
		p2.setLastName("Jane");
		p2.setContactNumber("798-897-9987");
		p2.setActive(true);
		
		p2.setAddressLine1("Brookyln Street");
		p2.setAddressLine2("1037, NY EXE");
		p2.setCity("New York");
		p2.setCountry("USA");
		p2.setPostalCode("654790");
	
		
		patientService.add(p2);
		//a2.setPatientId(uuid2);
		//addressService.addAddressToRepo(a2);
		Patient p3 = new Patient();
		
		final String uuid3 = UUID.randomUUID().toString().replace("-", "");
		System.out.println(uuid3);
		p3.setId(uuid3);
		p3.setFirstName("Tom");
		p3.setLastName("Jenkims");
		p3.setContactNumber("484-541-2121");
		p3.setActive(false);
		p3.setAddressLine1("Brookyln Street");
		p3.setAddressLine2("1037, NY EXE");
		p3.setCity("New York");
		p3.setCountry("USA");
		p3.setPostalCode("654790");
	
		patientService.add(p3);
		//a3.setPatientId(uuid3);
		//addressService.addAddressToRepo(a3);
		
		Patient p4 = new Patient();
		final String uuid4 = UUID.randomUUID().toString().replace("-", "");
		System.out.println(uuid4);
		p4.setId(uuid4);
		p4.setFirstName("Spider");
		p4.setLastName("Doe");
		p4.setContactNumber("335-353-4789");
		p4.setActive(true);
		p4.setAddressLine1("Brookyln Street");
		p4.setAddressLine2("1037, NY EXE");
		p4.setCity("New York");
		p4.setCountry("USA");
		p4.setPostalCode("654790");
	
		
		patientService.add(p4);
		
		
		//a4.setPatientId(uuid4);
		//addressService.addAddressToRepo(a4);
		
		Patient p5 = new Patient();
		final String uuid5 = UUID.randomUUID().toString().replace("-", "");
		System.out.println(uuid5);
		p5.setId(uuid5);
		p5.setFirstName("Antman");
		p5.setLastName("Doe");
		p5.setContactNumber("454-543-5351");
		p5.setActive(true);
		p5.setAddressLine1("Brookyln Street");
		p5.setAddressLine2("1037, NY EXE");
		p5.setCity("New York");
		p5.setCountry("USA");
		p5.setPostalCode("654790");
	
		patientService.add(p5);
		
		//a5.setPatientId(uuid5);
		//addressService.addAddressToRepo(a5);
	}
}