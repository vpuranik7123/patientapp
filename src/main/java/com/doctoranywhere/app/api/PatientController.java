package com.doctoranywhere.app.api;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.doctoranywhere.app.manager.PatientManager;
import com.doctoranywhere.app.model.Patient;
import com.doctoranywhere.app.model.PatientVo;

@RestController
public class PatientController extends BaseController {

	@Autowired
	PatientManager manager;


	@RequestMapping(value = "/patient/add", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public void addPatient(@RequestBody String patientJson) throws Exception{
	 System.out.println(patientJson);
	 manager.add(patientJson);
	}
	
	
	@RequestMapping(value = "/patient/get/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Patient getPatient(@PathVariable(value = "id")String patientId) throws Exception{
	 return manager.getPatient(patientId);
	}
	
	@RequestMapping(value = "/patient/edit", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public void editPatient(@RequestBody Patient patient) throws Exception{
	 manager.edit(patient);
	}
	
	
	@RequestMapping(value = "/patient/softdelete/{id}", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void softDeletePatient(@PathVariable(value = "id")String patientId) throws Exception{
	manager.softdelete(patientId);
	}
	
	
	@RequestMapping(value = "/patient/delete/{id}", method = RequestMethod.DELETE)
	public void deletePatient(@PathVariable(value = "id")String patientId) throws Exception{
		manager.delete(patientId);
	}
	
	
	@RequestMapping(value = "/patient/all", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<PatientVo> getPatients() throws Exception {
		return  manager.getPatientVo();
	}

	@RequestMapping(value = "/patient/getall", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Set<Patient> getAllPatients() throws Exception {
		return (manager.getAllPatients());
	}
	
	
	@RequestMapping(value = "/patient/get", params = { "page", "size" }, method = RequestMethod.GET)
	public Page<PatientVo> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) throws Exception {
		Page<PatientVo> resultPage = manager.findPaginated(page, size);
		if (page > resultPage.getTotalPages()) {
			throw new Exception();
		}
		return resultPage;
	}
	

}
