package com.doctoranywhere.app.repository;

import org.apache.ignite.springdata.repository.IgniteRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.doctoranywhere.app.model.Address;

/*
 * 
 * This class was created to support multiple address for a single patient, 
 * currently. UI supports only one address for one patient. So, this is rendered as moot as of now.
 * 
 */

@NoRepositoryBean
//@RepositoryConfig(cacheName = "addressRepository")
public interface AddressRepository extends IgniteRepository<Address, String> {

}
