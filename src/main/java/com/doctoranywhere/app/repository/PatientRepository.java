package com.doctoranywhere.app.repository;

import org.apache.ignite.springdata.repository.IgniteRepository;
import org.apache.ignite.springdata.repository.config.RepositoryConfig;
import org.springframework.data.repository.NoRepositoryBean;

import com.doctoranywhere.app.model.Patient;


@NoRepositoryBean
@RepositoryConfig(cacheName = "patientRepository")
public interface PatientRepository extends IgniteRepository<Patient, String>{

}
