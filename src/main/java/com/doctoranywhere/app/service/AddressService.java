package com.doctoranywhere.app.service;

import java.util.Set;

import org.springframework.stereotype.Service;

import com.doctoranywhere.app.model.Address;
/*
 * 
 * This class was created to support multiple address for a single patient, 
 * currently. UI supports only one address for one patient. So, this is rendered moot as of now.
 * 
 */
@Service
public interface AddressService {
	
	void addAddressToRepo(Address address);
	
	Set<Address> retrievePatientAddress(String patientId);

}
