package com.doctoranywhere.app.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.ignite.Ignite;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doctoranywhere.app.model.Address;
/*
 * 
 * This class was created to support multiple address for a single patient, 
 * currently. UI supports only one address for one patient. So, this is rendered moot as of now.
 * 
 */
@Service
public class AddressServiceImpl implements AddressService {
	private Logger logger =  LoggerFactory.getLogger(this.getClass());

	  @Autowired
	  Ignite ignite;
	
	
	@Override
	public void addAddressToRepo(Address address) {
		address.setAddressId(UUID.randomUUID().toString().replace("-", ""));
		ignite.getOrCreateCache("addressRepository").put(address.getAddressId(),address);
	}
	
	
	public Set<Address> retrievePatientAddress(String patientId){
		
		Set<Address> address = new HashSet<Address>();
		QueryCursor<List<?>> cursor = ignite.getOrCreateCache("addressRepository").query(new SqlFieldsQuery("select id, addressLine1, addressLine2, city, country, postalCode from address where patientId = ?").setArgs(patientId));
		
		for (List<?> row : cursor) {
			Address a = new Address();
			a.setAddressId(row.get(0).toString());
			if(row.get(1) != null)
			a.setAddressLine1(row.get(1).toString());
			if(row.get(2) != null)
			a.setAddressLine2(row.get(2).toString());
			if(row.get(3) != null)
			a.setCity(row.get(3).toString());
			if(row.get(4) != null)
			a.setCountry(row.get(4).toString());
			if(row.get(5) != null)
			a.setPostalCode(row.get(5).toString());
			a.setPatientId(patientId);
			address.add(a);
		}
		
		return address;
	}
	
	
	

}
