package com.doctoranywhere.app.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.ignite.Ignite;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doctoranywhere.app.model.Patient;

@Service
public class PatientServiceImpl implements PatientService{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	 @Autowired
     Ignite ignite;

	@Override
	public Set<Patient> getAllPatients() throws Exception {
		
		logger.info("> getAllPatients");
		Set<Patient> patients  = new HashSet<Patient>();

		QueryCursor<List<?>> cursor = ignite.getOrCreateCache("patientRepository").query(new SqlFieldsQuery("select id, firstName, lastName, contactNumber, isActive, addressLine1, addressLine2, city, country, postalCode from Patient order by firstName"));
		for (List<?> row : cursor) {
			Patient patient = new Patient();
			patient.setId(row.get(0).toString());
			patient.setFirstName(row.get(1).toString());
			patient.setLastName(row.get(2).toString());
			patient.setContactNumber(row.get(3).toString());
			patient.setActive(Boolean.valueOf(row.get(4).toString()));
			if(row.get(5)!=null)
			patient.setAddressLine1(row.get(5).toString());
			if(row.get(6)!=null)
			patient.setAddressLine2(row.get(6).toString());
			if(row.get(7)!=null)
			patient.setCity(row.get(7).toString());
			if(row.get(8)!=null)
			patient.setCountry(row.get(8).toString());
			if(row.get(9)!=null)
			patient.setPostalCode(row.get(9).toString());
			patients.add(patient);
		}	
		logger.info("< getAllPatients");
	    return patients;
	}

	
	@Override
	public Patient getPatient(String patientId) {
		QueryCursor<List<?>> cursor = ignite.getOrCreateCache("patientRepository").query(new SqlFieldsQuery("select id, firstName, lastName, contactNumber, isActive from Patient where id=?").setArgs(patientId));
		Patient patient = new Patient();
		for (List<?> row : cursor) {
			patient.setId(row.get(0).toString());
			patient.setFirstName(row.get(1).toString());
			patient.setLastName(row.get(2).toString());
			patient.setContactNumber(row.get(3).toString());
			patient.setActive(Boolean.valueOf(row.get(4).toString()));
		}	
		return patient;
	}
	

}
