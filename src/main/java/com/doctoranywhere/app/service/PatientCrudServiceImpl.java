package com.doctoranywhere.app.service;

import java.util.UUID;

import org.apache.ignite.Ignite;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doctoranywhere.app.model.Patient;


@Service
public class PatientCrudServiceImpl implements PatientCrudService{

	private Logger logger =  LoggerFactory.getLogger(this.getClass());
	
	 @Autowired
	 Ignite ignite;
	
	@Override
	public void add(Patient patient) throws Exception {
		String uuid= UUID.randomUUID().toString().replace("-","");
		patient.setId(uuid);
		ignite.getOrCreateCache("patientRepository").put(uuid, patient);
	}

	@Override
	public void edit(Patient patient) throws Exception {
		logger.info("> edit");
		ignite.getOrCreateCache("patientRepository").query(new SqlFieldsQuery("update Patient set firstName = ?,lastName=?, contactNumber = ?,addressLine1 =?,addressLine2=?, city = ? , country = ? , postalCode=? where id = ?").
				setArgs(patient.getFirstName(),patient.getLastName(),patient.getContactNumber(),patient.getAddressLine1(),patient.getAddressLine2(),patient.getCity(),patient.getCountry(),patient.getPostalCode(),patient.getId()));
		
		logger.info("< edit");
		
	}

	@Override
	public void softdelete(String patientId) throws Exception {
		logger.info("> softdelete");
		ignite.getOrCreateCache("patientRepository").query(new SqlFieldsQuery("update Patient set isActive = ? where id = ?").setArgs(false,patientId));
		logger.info("< softdelete");
	}

	@Override
	public void delete(String patientId) throws Exception {
		logger.info("> delete");
		ignite.getOrCreateCache("patientRepository").query(new SqlFieldsQuery("delete from Patient where id = ?").setArgs(patientId));
		logger.info("< delete");
	}
	
}
