package com.doctoranywhere.app.service;

import java.util.Set;

import com.doctoranywhere.app.model.Patient;

public interface PatientService {
	
	Set<Patient> getAllPatients() throws Exception;

	Patient getPatient(String patientId);
	

}
