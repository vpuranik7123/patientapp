package com.doctoranywhere.app.service;

import com.doctoranywhere.app.model.Patient;

public interface PatientCrudService {
	
	void add(Patient patient) throws Exception;
	void edit(Patient patient) throws Exception;
	void softdelete(String patientId) throws Exception;
	void delete(String patientId) throws Exception;
	
	

}
