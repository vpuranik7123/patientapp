package com.doctoranywhere.app.util;

import java.io.IOException;

import com.doctoranywhere.app.model.Address;
import com.doctoranywhere.app.model.Patient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AppUtil {
	
	public static Patient convertJSONToPatientObject(String jsonString) throws JsonParseException, JsonMappingException, IOException {
		return  new ObjectMapper().readValue(jsonString, Patient.class);
	}

	public static Address convertJSONToAddressObject(String jsonString) throws JsonParseException, JsonMappingException, IOException {
		return  new ObjectMapper().readValue(jsonString, Address.class);
	}
	
/*	public static PatientVo convertJSONToPatientVo(String jsonString) throws JsonParseException, JsonMappingException, IOException {
		PatientVo patientVo = new PatientVo();
		String[] s = jsonString.split("#");
		Set<Address> address = new HashSet<Address>();
		patientVo.setPatient(convertJSONToPatientObject(s[0]));
		address.add(convertJSONToAddressObject(s[1]));
		patientVo.setAddress(address);
		return patientVo;
	}*/
}
