package com.doctoranywhere.app.manager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.doctoranywhere.app.model.Patient;
import com.doctoranywhere.app.model.PatientVo;
import com.doctoranywhere.app.service.AddressService;
import com.doctoranywhere.app.service.PatientCrudService;
import com.doctoranywhere.app.service.PatientService;
import com.doctoranywhere.app.util.AppUtil;

@Service
public class PatientManager {
	
	private Logger logger =  LoggerFactory.getLogger(this.getClass());

	@Autowired
	PatientService patientService;
	
	@Autowired
	AddressService addressService;

	@Autowired
	PatientCrudService patientCrudService;
	
	
	public void add(String patientJson) throws Exception{
	
		patientCrudService.add(AppUtil.convertJSONToPatientObject(patientJson));
		
		
	}
	
	public void edit(Patient patient) throws Exception{
		patientCrudService.edit(patient);
	}
	
	public void softdelete(String patientId) throws Exception{
		patientCrudService.softdelete(patientId);
	}
	
	public void delete(String patientId) throws Exception{
		patientCrudService.delete(patientId);
	}
	
	public Set<Patient> getAllPatients() throws Exception {
		return patientService.getAllPatients();
		
	}
	
	public Page<PatientVo> findPaginated(int page, int size) throws Exception {
		Pageable pageable = new PageRequest(page, size);
		List<PatientVo> patientVo = new ArrayList<>(getPatientVo());
		return new PageImpl(patientVo,pageable,patientVo.size());
	 }

	public Patient getPatient(String patientId) {
		return patientService.getPatient(patientId);
		
	}
	
	public Set<PatientVo> getPatientVo() throws Exception {
		Set<PatientVo> patientVo = new HashSet();
		Set<Patient> patientSet = patientService.getAllPatients();
	
		for(Patient p : patientSet) {
			PatientVo vo = new PatientVo();
			vo.setPatient(p);
			vo.setAddress(addressService.retrievePatientAddress(p.getId()));
			patientVo.add(vo);
		}
		System.out.println(patientSet.size());
		System.out.println(patientVo.size());
		return patientVo;
	}	 
	

}
