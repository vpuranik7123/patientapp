package com.doctoranywhere.app;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.springdata.repository.config.EnableIgniteRepositories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.doctoranywhere.app.model.Address;
import com.doctoranywhere.app.model.Patient;

@Configuration
@EnableIgniteRepositories("com.doctoranywhere.app.repository")
public class SpringDataConfig {

	public static Ignite ignite;

	@Bean
	public Ignite igniteInstance() {

		IgniteConfiguration cfg = new IgniteConfiguration();
		cfg.setIgniteInstanceName("ignite");
		CacheConfiguration ccfgPatientRepo = new CacheConfiguration("patientRepository");
		ccfgPatientRepo.setIndexedTypes(String.class, Patient.class);
		ccfgPatientRepo.setCopyOnRead(false); 
	
		/*
		 * currently. UI supports only one address for one patient. So, 'addressRepository' is rendered moot as of now.
		 */
		CacheConfiguration ccfgAddressRepo = new CacheConfiguration("addressRepository");
		ccfgAddressRepo.setIndexedTypes(String.class, Address.class);
		cfg.setCacheConfiguration(new CacheConfiguration[] { ccfgPatientRepo,ccfgAddressRepo });
		ignite = Ignition.start(cfg);
		return ignite;
	}

	
	

}
