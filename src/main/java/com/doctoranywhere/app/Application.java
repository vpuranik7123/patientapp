package com.doctoranywhere.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.*;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		System.out.println("asdasd");
		System.out.println(rearrangeString("abbbb", 2));
		System.out.println(reorganizeString("abbbb"));
		System.out.println(reorganizeString1("abbbb"));
	}

	public static String rearrangeString(String str, int k) {
		Map<Character, Integer> map = new HashMap<>();
		for (char c : str.toCharArray()) {
			map.put(c, map.getOrDefault(c, 0) + 1); // char freq
		}
		Queue<Map.Entry<Character, Integer>> q = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue()); // PriorityQueue,
																												// order
																												// by
																												// freq
		q.addAll(map.entrySet());
		StringBuilder sb = new StringBuilder();
		List<Map.Entry<Character, Integer>> waitList = new LinkedList();
		while (!q.isEmpty()) { // q hold all currently available (ch, freq) pairs, all non-valid ones r in
								// waitList
			Map.Entry<Character, Integer> entry = q.poll();
			char ch = entry.getKey();
			int freq = entry.getValue();
			sb.append(ch); // append to result
			entry.setValue(freq - 1);
			waitList.add(entry);
			if (sb.length() >= k) { // passed intial k chars, waitList full, start to release // passed in
				Map.Entry<Character, Integer> front = waitList.remove(0); // release 1st (char, freq) from waitlist and
																			// add back to pq if freq still > 0
				if (front.getValue() > 0)
					q.offer(front); // released ch might not be available any more (all freq used up)
			}
		}
		return sb.toString(); // (ch, freq) stuck in waitList
	}

	public static String reorganizeString(String S) {
		int N = S.length();
		int[] count = new int[26];
		for (char c : S.toCharArray())
			count[c - 'a']++;
		PriorityQueue<MultiChar> pq = new PriorityQueue<MultiChar>(
				(a, b) -> a.count == b.count ? a.letter - b.letter : b.count - a.count);

		for (int i = 0; i < 26; ++i)
			if (count[i] > 0) {
				if (count[i] > (N + 1) / 2)
					return "";
				pq.add(new MultiChar(count[i], (char) ('a' + i)));
			}

		StringBuilder ans = new StringBuilder();
		while (pq.size() >= 2) {
			MultiChar mc1 = pq.poll();
			MultiChar mc2 = pq.poll();
			ans.append(mc1.letter);
			ans.append(mc2.letter);
			if (--mc1.count > 0)
				pq.add(mc1);
			if (--mc2.count > 0)
				pq.add(mc2);
		}

		if (pq.size() > 0)
			ans.append(pq.poll().letter);
		return ans.toString();
	}

	public static String reorganizeString1(String S) {
		int N = S.length();
		int[] counts = new int[2];
		for (char c : S.toCharArray())
			counts[c - 'a'] += 100;
		for (int i = 0; i < 2; ++i)
			counts[i] += i;
		// Encoded counts[i] = 100*(actual count) + (i)
		Arrays.sort(counts);

		char[] ans = new char[N];
		int t = 1;
		for (int code : counts) {

			int ct = code / 100;
			System.out.println("ct: " + ct);
			char ch = (char) ('a' + (code % 100));
			System.out.println(ch);

			System.out.println(ct > (N + 1) / 2);
			if (ct > (N + 1) / 2) {
				System.out.println("inside if loop ct" + ct);

				if (t >= N)
					t = 0;
				ans[t] = ch;
				t = t + 2;

			} else {
				for (int i = 0; i < ct; ++i) {

					System.out.println("inside for loop ct" + ct);
					if (t >= N)
						t = 0;
					ans[t] = ch;
					t += 2;
				}
			}
		}

		return String.valueOf(ans);
	}
	
	
	public static String solutio1n(int[] T) {
		
		if(T.length % 4 == 0) {
			
			int d = T.length/4;
			
			
			int[] winter = new int[d];
			int[] spring = new int[d];
			int[] summer = new int[d];
			int[] autumn = new int[d];
			
			int i = 0;
			while(i < d) {
				winter[i] = T[i];
				i++;
			}
			
			int j=0;
			while(i < d*2) {
				spring[j] = T[i];
				i++;
				j++;
			}
			int k=0;
			while(i < d*3) {
				summer[k] = T[i];
				i++;k++;
			}
			int l=0;
			while(i < d*4) {
				summer[l] = T[i];
				i++;l++;
			}
		}
		
		
		return null;
		
	}

}

class MultiChar {
	int count;
	char letter;

	MultiChar(int ct, char ch) {
		count = ct;
		letter = ch;
	}
}
