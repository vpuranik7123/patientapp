export class Patient {
    id:string; /*uuid*/
    firstName:string;
    lastName:string;
    contactNumber:number;
    active:boolean;
    addressLine1:string;
    addressLine2:string;
    city:string;
    country:string;
    postalCode:string;

}
