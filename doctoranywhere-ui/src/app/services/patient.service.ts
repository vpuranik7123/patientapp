import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Patient } from '../patient';
//import { Address } from '../address';



@Injectable()
export class PatientService {

  private patient:Patient;
 // private address:Address;
  constructor(private http:HttpClient) { }
  
  getPatients(){
  let token = localStorage.getItem('access_token');  
  return this.http.get('/server/patient/getall',{headers: new HttpHeaders().set('Authorization','Bearer '+ token)});
  }
  
  getPatient(id:string){
    let token = localStorage.getItem('access_token');  
  	return this.http.get('/server/patient/get/' + id,{headers: new HttpHeaders().set('Authorization','Bearer '+ token)});
  }
  
  addPatient(p:Patient){
  let token = localStorage.getItem('access_token');  
  let body = JSON.stringify(p); 
  alert(body);
  return this.http.post('/server/patient/add',body,{headers: new HttpHeaders({'Content-Type':'application/json'}).set('Authorization','Bearer '+ token)});
  }


  editPatient(p:Patient){
    let token = localStorage.getItem('access_token');  
    alert(JSON.stringify(p));
    return this.http.put('/server/patient/edit',p,{headers: new HttpHeaders().set('Authorization','Bearer '+ token)});
    }


  getPatientInfo(p:any,s:any){
    return this.http.get('/server/patient/get',{params : {page:p,size:s}},);
  }

  softDelete(p:any){
  let token = localStorage.getItem('access_token'); 
   return this.http.post('/server/patient/softdelete/' +p.id,p,{headers: new HttpHeaders().set('Authorization','Bearer '+ token)});
  }

  deletePatient(id:string){
    let token = localStorage.getItem('access_token'); 
    return this.http.delete('/server/patient/delete/' + id,{headers: new HttpHeaders().set('Authorization','Bearer '+ token)});
  }
  
  setter(patient:Patient){
    this.patient=patient;
  }

  getter(){
    return this.patient;
  }

  /*setAddress(address:Address){
    this.address=address;
  }

  getAddress(){
    return this.address;
  } */
}
