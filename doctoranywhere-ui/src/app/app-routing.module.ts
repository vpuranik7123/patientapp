import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PatientsComponent} from './components/patients/patients.component';
import {AddPatientComponent} from './components/patient/add-patient.component';
import { CallbackComponent } from './components/callback/callback.component';
import {AuthGuard} from './services/auth.guard';
import { EditPatientComponent } from './components/edit-patient/edit-patient.component';

const routes: Routes = [
{
	path:'add',
	component: AddPatientComponent
},{
	path: 'edit',
	component: EditPatientComponent
},
{	path: 'patients',
	component: PatientsComponent,
	canActivate:[AuthGuard]
},{
	path:'callback',
	component: CallbackComponent
}
];

@NgModule({
imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})

export class AppRoutingModule { }