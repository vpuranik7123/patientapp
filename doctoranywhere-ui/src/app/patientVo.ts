import { Patient } from "./patient";
import { Address } from "./address";

export class PatientVo {
    private patient:Patient;
    private addresses:Address[]=[];
}
