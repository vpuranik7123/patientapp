import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../services/patient.service';
import { Patient } from '../../patient';
import {Router} from '@angular/router';
import { Address } from '../../address';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {

public patients;

constructor(private patientService:PatientService,private router:Router) { }

  ngOnInit() {
    this.getPatients();
    
  }
  
  getPatients(){
  	this.patientService.getPatients().subscribe(
    (data:any)=> { 
      console.log(JSON.stringify(data));
      this.patients = data;
    },
  	err => console.error(err),
  	() => console.log('patients loaded')
  	);
  }

  softDelete(patient:Patient){
    alert('Are you sure you want to soft delete? patient '+patient.firstName);
    this.patientService.softDelete(patient.id).subscribe(
      (data:any) => {
      },
      err => console.error(err),
      () => console.log('soft delete successful')
    );
  }

  delete(p:Patient){
    alert('Are you sure you want to  delete?' +p.firstName);
    this.patientService.deletePatient(p.id).subscribe(
      (data:any) => {
       this.patients.splice(this.patients.indexOf(p),1);
      },
      err => console.error(err),
      () => console.log(' delete successful')
    );
  }

  edit(p:Patient)
  {
    this.patientService.setter(p);
    this.router.navigate(['/edit']);
  }

  newPatient(){
    let patient = new Patient();
    this.patientService.setter(patient);
    this.router.navigate(['/add']);
  }


}
