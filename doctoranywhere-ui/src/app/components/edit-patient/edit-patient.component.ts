import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../services/patient.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable,of} from 'rxjs';
import { Patient } from '../../patient';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit-patient',
  templateUrl: './edit-patient.component.html',
  styleUrls: ['./edit-patient.component.css']
})
export class EditPatientComponent implements OnInit {

  patientform: FormGroup;
  validMessage: string = "";
  public patient:Patient;
  constructor(private patientService: PatientService,private router:Router) { }

  ngOnInit() {
  this.patient = this.patientService.getter();
  this.patientform = new FormGroup({
  id: new FormControl('', Validators.required),
  firstName: new FormControl('',Validators.required),
  lastName: new FormControl('', Validators.required),
  contactNumber: new FormControl('',Validators.required),
  addressLine1: new FormControl('',Validators.required),
  addressLine2: new FormControl('',Validators.required),
  country: new FormControl('',Validators.required),
  city:new FormControl('',Validators.required),
  postalCode:new FormControl('',Validators.required)
  });
}
  
  submitEdit(){
  if(this.patientform.valid){
  this.validMessage = "Patient Edited, thank you!";
  this.patientService.editPatient(this.patientform.value).subscribe(
  data => {
  return true;
  },
  error => {
  return Observable.throw(error);
  })
  }else{
    this.validMessage = "Please fill out the form before submitting!"
  }
  }

  back(){
    this.router.navigate(['/patients']);
  }
}