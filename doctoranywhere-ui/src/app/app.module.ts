import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatientService} from './services/patient.service';
import { PatientsComponent } from './components/patients/patients.component';
import { AddPatientComponent } from './components/patient/add-patient.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ClarityModule} from '@clr/angular';
import { CallbackComponent } from './components/callback/callback.component';
import {AuthService} from './services/auth.service';
import {AuthGuard} from './services/auth.guard';
import { EditPatientComponent } from './components/edit-patient/edit-patient.component';

@NgModule({
  declarations: [
    AppComponent,
    PatientsComponent,
    AddPatientComponent,
    CallbackComponent,
    EditPatientComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,AppRoutingModule,ReactiveFormsModule,NgxDatatableModule, ClarityModule.forChild()
  ],
  providers: [PatientService,AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
